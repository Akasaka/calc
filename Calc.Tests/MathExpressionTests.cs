using System;
using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;

namespace Calc.Tests
{
    public class MathExpressionTests
    {
        [SetUp]
        public void Setup()
        {
            CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
        }

        public static IEnumerable<object[]> GetCases()
        {
            yield return new object[] { "-1 + 1", 0d, true };
            yield return new object[] { "1 + 1", 2d, true };
            yield return new object[] { "1 * 2", 2d, true };
            yield return new object[] { "2 * -1", -2d, true };
            yield return new object[] { "2 / 2", 1d, true };
            yield return new object[] { "5 / 2", 2.5d, true };
            yield return new object[] { @"5 \ 2", 2d, true };
            yield return new object[] { @"5 % 2", 1d, true };
            yield return new object[] { @"5 ^ 2", 25d, true };
            yield return new object[] { @"2(3 + 2)", 10d, true };
            yield return new object[] { @"2 * (3 + 2)", 10d, true };
            yield return new object[] { @"2 * (-3 + 7)", 8d, true };
            yield return new object[] { @"2 * (3 + 2) + 6", 16d, true};
            yield return new object[] { @"(2 * (3 + 2) + 2) * 2", 24d, true };
            yield return new object[] { @"8.2 + (0.2 * 2) * 2", 9d, true };
            yield return new object[] { @"(8.2 + (0.2 * 2) * 2) * -1", -9d, true };
            yield return new object[] { @"1 + 1)", 0d, false };
            yield return new object[] { @"1 + 1()", 0d, false };
            yield return new object[] { @"1 + 1))", 0d, false };
            yield return new object[] { @"1 + 6 + (1 + 4) ** 3", 0d, false };
            yield return new object[] { @"2 / 0", 0d, false };
            yield return new object[] { @"2 / (2 - 2^1)", 0d, false };
        }

        [Test]
        [TestCaseSource(nameof(GetCases))]
        public void Test(string expStr, double expectedResult, bool expected)
        {
            var actual = TestCase(expStr, out var result);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedResult, result);
        }

        public static bool TestCase(string expStr, out double result)
        {
            result = 0;
            try
            {
                var exp = new MathExpression(expStr);
                result = exp.ParseExp().Eval();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}