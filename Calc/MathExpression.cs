﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Calc.Operations;

namespace Calc
{
    public class MathExpression
    {
        private readonly Dictionary<char, Type> _level2Operators = new Dictionary<char, Type>
        {
            {'+', typeof(Plus)},
            {'-', typeof(Minus)}
        };

        private readonly Dictionary<char, Type> _level1Operators = new Dictionary<char, Type>
        {
            {'*', typeof(Multiply)},
            {'/', typeof(Divide)},
            {'\\', typeof(DivideInt)},
            {'%', typeof(Mod)},
            {'^', typeof(Pow)}
        };

        private int _currentPos;

        public MathExpression(string expStr)
        {
            if (expStr == null)
                throw new ArgumentNullException(nameof(expStr));

            if (expStr == string.Empty)
                throw new ArgumentException(nameof(expStr));

            ExpressionStr = FixStr(expStr);
        }

        private char CurrentChar => ExpressionStr[_currentPos];

        public string ExpressionStr { get; }

        public CultureInfo CurrentProvider { get; } = CultureInfo.CurrentCulture;

        public char CurrencyDecimalSeparator => CurrentProvider.NumberFormat.CurrencyDecimalSeparator[0];

        public Operation ParseExp()
        {
            _currentPos = 0;
            var root = ParsePriorityLevel2();

            if (_currentPos != ExpressionStr.Length)
            {
                var op = CurrentChar;

                if (op == ')')
                    throw new PosException(_currentPos, $"Обнаружена лишняя скобка на позиции {_currentPos + 1}");
                throw new PosException(_currentPos, $"Неизвестный операнд на позиции {_currentPos + 1}");
            }

            return root ?? new Number(0);
        }

        private Operation ParsePriorityLevel2()
        {
            return ParseLevel(_level2Operators, ParsePriorityLevel1);
        }

        private Operation ParsePriorityLevel1()
        {
            return ParseLevel(_level1Operators, ParsePriorityLevel0);
        }

        private Operation ParseLevel(Dictionary<char, Type> levelOperations, Func<Operation> innerParseLevel)
        {
            var result = innerParseLevel();
            while (true)
            {
                var isMatched = false;
                foreach (var operation in levelOperations)
                    if (Match(operation.Key))
                    {
                        var rightOp = innerParseLevel();
                        result = (Operation) Activator.CreateInstance(operation.Value, result, rightOp);
                        isMatched = true;
                    }

                if (!isMatched)
                    return result;
            }
        }

        private Operation ParsePriorityLevel0()
        {
            Operation result;

            if (Match('('))
            {
                result = ParsePriorityLevel2();
                if (!Match(')'))
                    throw new PosException(_currentPos, "Не обнаружено закрывающей скобки");
            }
            else
            {
                var isNegation = Match('-');

                var startIndex = _currentPos;
                while (_currentPos < ExpressionStr.Length &&
                       (char.IsDigit(CurrentChar) || CurrentChar == CurrencyDecimalSeparator))
                    ++_currentPos;

                var strVal = ExpressionStr.Substring(startIndex, _currentPos - startIndex);

                if (!double.TryParse(strVal, NumberStyles.Any, CurrentProvider, out var val))
                    throw new PosException(startIndex, $"Ошибка разбора, позиция {startIndex + 1}");

                if (isNegation)
                    val = -val;

                result = new Number(val);
            }

            return result;
        }

        private bool Match(char ch)
        {
            if (_currentPos >= ExpressionStr.Length)
                return false;

            if (CurrentChar == ch)
            {
                _currentPos++;
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Выгравнивание строки выражения
        /// </summary>
        /// <param name="str">Выражение</param>
        /// <returns>Форматированная строка вырожения</returns>
        internal static string FixStr(string str)
        {
            var sb = new StringBuilder();
            var isLastDigit = false;
            foreach (var c in str)
            {
                if (c == ' ')
                    continue;

                if (c == '(' && isLastDigit)
                {
                    isLastDigit = false;
                    sb.Append("*");
                }
                else if (char.IsLetter(c))
                {
                    isLastDigit = false;
                }
                else if (char.IsSymbol(c))
                {
                    isLastDigit = false;
                }
                else if (char.IsPunctuation(c))
                {
                    isLastDigit = false;
                }
                else if (char.IsDigit(c))
                {
                    isLastDigit = true;
                }

                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}