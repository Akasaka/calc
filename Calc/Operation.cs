﻿namespace Calc
{
    public abstract class Operation
    {
        public abstract double Eval();
    }
}