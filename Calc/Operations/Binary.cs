﻿namespace Calc.Operations
{
    public abstract class Binary : Operation
    {
        public Binary(Operation leftOp, Operation rightOp)
        {
            LeftOp = leftOp;
            RightOp = rightOp;
        }

        protected Operation LeftOp { get; }

        protected Operation RightOp { get; }
    }
}