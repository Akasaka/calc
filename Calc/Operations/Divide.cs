﻿using System;

namespace Calc.Operations
{
    internal class Divide : Binary
    {
        public Divide(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            var rightEval = RightOp.Eval();
            if (rightEval == 0.0d)
                throw new DivideByZeroException();
            return LeftOp.Eval() / rightEval;
        }
    }
}