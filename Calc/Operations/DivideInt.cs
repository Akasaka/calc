﻿using System;

namespace Calc.Operations
{
    internal class DivideInt : Binary
    {
        public DivideInt(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            var rightEval = RightOp.Eval();
            if (rightEval == 0.0d)
                throw new DivideByZeroException();
            return (int) (LeftOp.Eval() / rightEval);
        }
    }
}