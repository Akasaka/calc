﻿namespace Calc.Operations
{
    public class Minus : Binary
    {
        public Minus(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            return LeftOp.Eval() - RightOp.Eval();
        }
    }
}