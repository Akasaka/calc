﻿using System;

namespace Calc.Operations
{
    internal class Mod : Binary
    {
        public Mod(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            var rightEval = RightOp.Eval();
            if (rightEval == 0.0d)
                throw new DivideByZeroException();
            return LeftOp.Eval() % rightEval;
        }
    }
}