﻿namespace Calc.Operations
{
    public class Multiply : Binary
    {
        public Multiply(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            return LeftOp.Eval() * RightOp.Eval();
        }
    }
}