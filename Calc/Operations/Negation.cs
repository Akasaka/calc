﻿namespace Calc.Operations
{
    public class Negation : Unary
    {
        public Negation(Operation n) : base(n)
        {
        }

        public override double Eval()
        {
            return -Op.Eval();
        }
    }
}