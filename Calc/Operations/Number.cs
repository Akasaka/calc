﻿namespace Calc.Operations
{
    public class Number : Operation
    {
        private readonly double _value;

        public Number(double val)
        {
            _value = val;
        }

        public override double Eval()
        {
            return _value;
        }
    }
}