﻿namespace Calc.Operations
{
    public class Plus : Binary
    {
        public Plus(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            return LeftOp.Eval() + RightOp.Eval();
        }
    }
}