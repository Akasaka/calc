﻿using System;

namespace Calc.Operations
{
    public class Pow : Binary
    {
        public Pow(Operation leftOp, Operation rightOp) : base(leftOp, rightOp)
        {
        }

        public override double Eval()
        {
            return Math.Pow(LeftOp.Eval(), RightOp.Eval());
        }
    }
}