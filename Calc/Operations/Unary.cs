﻿namespace Calc.Operations
{
    public abstract class Unary : Operation
    {
        public Unary(Operation op)
        {
            Op = op;
        }

        protected Operation Op { get; }
    }
}