﻿using System;

namespace Calc
{
    public class PosException : Exception
    {
        public PosException(int pos, string msg) : base(msg)
        {
            Pos = pos;
        }

        public int Pos { get; }
    }
}