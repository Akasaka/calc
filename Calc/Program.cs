﻿using System;
using System.Data;
using System.Globalization;

namespace Calc
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Вариант реализации:");
            Console.WriteLine("1. Самописный");
            Console.WriteLine("2. Читерский");

            while (true)
            {
                var key = Console.ReadKey();

                if (key.KeyChar == '1')
                    Custom();
                else if (key.KeyChar == '2')
                    CheatMode();
            }
        }

        private static void Custom()
        {
            Console.Clear();
            Console.WriteLine(
                $"Разделитель дробной части текущей культуры: {CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator}");
            Console.WriteLine();
            while (true)
            {
                Console.Write("Выражение: ");
                var buf = Console.ReadLine();

                var res = 0.0d;
                MathExpression exp = null;
                try
                {
                    exp = new MathExpression(buf);
                    res = exp.ParseExp().Eval();
                }
                catch (PosException e)
                {
                    Console.WriteLine($"Ошибка: {e.Message}");
                    Console.WriteLine(exp.ExpressionStr);
                    Console.SetCursorPosition(e.Pos, Console.CursorTop);
                    Console.WriteLine("^");
                    continue;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка: {e.Message}");
                    continue;
                }

                Console.WriteLine($"Результат: {res}");
                Console.WriteLine();
            }
        }

        private static void CheatMode()
        {
            Console.Clear();
            var dt = new DataTable();
            while (true)
            {
                Console.Write("Выражение: ");
                var str = Console.ReadLine();

                str = MathExpression.FixStr(str);
                object res;
                Exception ex = null;
                try
                {
                    res = dt.Compute(str, string.Empty);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка: {e.Message}");
                    continue;
                }

                Console.WriteLine($"Результат: {res}");
            }
        }
    }
}