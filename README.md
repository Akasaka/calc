### Калькулятор

[![pipeline status](https://gitlab.com/Akasaka/calc/badges/master/pipeline.svg)](https://gitlab.com/Akasaka/calc/-/commits/master)

Калькулятор имеет 2 реализации:
1. Самостоятельный разбор выражения
2. Использование встроенного решения на основе [`DataTable.Compute`](https://docs.microsoft.com/ru-ru/dotnet/api/system.data.datatable.compute)

Самописный вариант имеет операции:
- сложение
- вычитание
- умножение
- деление
- целочисленное деление (\\)
- возведение в степень (^)
- остаток от деление (%)

А так же имеет возможность отображение конкретной позициии проблемы в выражении.
